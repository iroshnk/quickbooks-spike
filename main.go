package main

import (
	"fmt"
	"log"
	"net/http"

	"quickbooks-spike/cache"
	"quickbooks-spike/expenses"
	"quickbooks-spike/xero"
)

func main() {
	//register static routes
	fs := http.FileServer(http.Dir("static/"))
	http.Handle("/", fs)
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.Handle("/connected-intuit/", http.StripPrefix("/connected-intuit/", http.FileServer(http.Dir("static/connected-intuit/"))))
	http.Handle("/connected-xero/", http.StripPrefix("/connected-xero/", http.FileServer(http.Dir("static/connected-xero/"))))

	//register handler routes
	http.HandleFunc("/oauth2redirect", Oauth2redirect)
	http.HandleFunc("/signInWithIntuit", SignInWithIntuit)
	http.HandleFunc("/refreshToken", RefreshToken)
	http.HandleFunc("/addExpense", AddExpense)
	http.HandleFunc("/getPurchase", GetPurchase)
	http.HandleFunc("/uploadAttachments", UploadAttachments)
	http.HandleFunc("/oauth2redirectXero", Oauth2redirectXero)
	http.HandleFunc("/signInWithXero", SignInWithXero)
	http.HandleFunc("/addXeroInvoice", AddXeroInvoice)
	http.HandleFunc("/uploadAttachmentsXero", UploadAttachmentsXero)
	http.HandleFunc("/refreshTokenXero", RefreshTokenXero)
	http.HandleFunc("/getInvoiceXero", GetInvoiceXero)

	//log and start server
	log.Println("running server on ", 9090)
	log.Fatal(http.ListenAndServe(":9090", nil))

}

func Oauth2redirect(w http.ResponseWriter, r *http.Request) {

	state := r.URL.Query().Get("state")
	code := r.URL.Query().Get("code")
	realmID := r.URL.Query().Get("realmId")
	cache.AddToCache("realmID", realmID)

	success, err := expenses.GetAccessToken(code, state)
	if success {
		http.Redirect(w, r, "/connected-intuit/", http.StatusFound)
	} else {
		log.Println(err)
	}
}

func Oauth2redirectXero(w http.ResponseWriter, r *http.Request) {

	state := r.URL.Query().Get("state")
	code := r.URL.Query().Get("code")
	//scope := r.URL.Query().Get("scope")
	//session_state := r.URL.Query().Get("session_state")

	success, err := xero.GetAccessToken(code, state)
	if success {
		http.Redirect(w, r, "/connected-xero/", http.StatusFound)
	} else {
		log.Println(err)
	}
}

func SignInWithIntuit(w http.ResponseWriter, r *http.Request) {
	log.Println("Inside SignInWithIntuit")
	http.Redirect(w, r, "https://appcenter.intuit.com/connect/oauth2?client_id=ABDsNeoQ3A076liDCJpPbda9GqYzQxRIanOPpGXsjunWOksEXr&redirect_uri=http%3A%2F%2Flocalhost%3A9090%2Foauth2redirect&response_type=code&scope=com.intuit.quickbooks.accounting&state=a461499a-647e-4c65-b84e-1abbd574cab4", http.StatusSeeOther)
}

func SignInWithXero(w http.ResponseWriter, r *http.Request) {
	log.Println("Inside SignInWithXero")
	http.Redirect(w, r, "https://login.xero.com/identity/connect/authorize?response_type=code&client_id=458DE830AA7A4E059CBE853C62BFAC23&redirect_uri=http%3A%2F%2Flocalhost%3A9090%2Foauth2redirectXero&scope=openid+profile+email+accounting.transactions+accounting.attachments&state=123", http.StatusSeeOther)
}

// RefreshToken to generate new tokens
func RefreshToken(w http.ResponseWriter, r *http.Request) {
	success, err := expenses.RefreshToken()
	if success {
		fmt.Fprintf(w, "Refresh Token success. ")
	} else {
		log.Println(err)
		fmt.Fprintf(w, "Error ...")
	}

	log.Println("Exiting RefreshToken ")
}

// RefreshToken to generate new tokens for xero
func RefreshTokenXero(w http.ResponseWriter, r *http.Request) {
	success, err := xero.RefreshToken()
	if success {
		fmt.Fprintf(w, "Refresh Token success ")
	} else {
		log.Println(err)
		fmt.Fprintf(w, "Error ...")
	}
}

func AddExpense(w http.ResponseWriter, r *http.Request) {
	accountRef := expenses.ReferenceType{}
	accountRef.Value = "42"
	accountRef.Name = "Visa"

	accountBasedRef := expenses.ReferenceType{}
	accountBasedRef.Value = "13"
	accountBasedRef.Name = "Meals and Entertainment"

	accountBasedExpenseLineDetail := expenses.AccountBasedExpenseLineDetail{}
	accountBasedExpenseLineDetail.AccountRef = accountBasedRef

	purchaseCreateLineApi := expenses.PurchaseCreateLineApi{}
	purchaseCreateLineApi.DetailType = "AccountBasedExpenseLineDetail"
	purchaseCreateLineApi.Amount = "11.0"
	purchaseCreateLineApi.AccountBasedExpenseLineDetail = accountBasedExpenseLineDetail

	var Line [1]expenses.PurchaseCreateLineApi
	Line[0] = purchaseCreateLineApi

	inv := expenses.PurchaseCreateApi{"CreditCard", accountRef, Line}
	client := &http.Client{}

	purchase, err := expenses.CreatePurchase(&inv, client)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Fprintf(w, "Expense Added. Expense Id : "+purchase.ID)
}

func GetPurchase(w http.ResponseWriter, r *http.Request) {
	purchase, err := expenses.GetPurchase()
	if err != nil {
		log.Fatal(err)
		fmt.Fprintf(w, "Purchase getting error! check the log. "+err.Error())
	} else {
		fmt.Fprintf(w, "Purchase - Purchase id : %s , Type : %s", purchase.ID, purchase.PaymentType)
	}
}

func UploadAttachments(w http.ResponseWriter, r *http.Request) {
	attachableResponse, err := expenses.UploadAttachments()
	if err != nil {
		log.Fatal(err)
		fmt.Fprintf(w, "Attachment not uploaded! check the log. "+err.Error())
	} else {
		fmt.Fprintf(w, "Attachment Added! - Attachment id : %s", attachableResponse.Attachable.Id)
	}
}

func AddXeroInvoice(w http.ResponseWriter, r *http.Request) {
	contact := xero.Contact{}
	contact.Name = "Xero Test"

	lineItem := xero.LineItem{}
	lineItem.Description = "Uber Kandy to Colombo"
	lineItem.AccountCode = "200"

	lineItem.Quantity = "1.00"
	lineItem.UnitAmount = "120.00"

	var lineItems [1]xero.LineItem
	lineItems[0] = lineItem

	createInvoiceApi := xero.CreateInvoiceApi{}
	createInvoiceApi.LineAmountTypes = "Exclusive"
	createInvoiceApi.Type = "ACCPAY"
	createInvoiceApi.Contact = contact
	//createInvoiceApi.Date = time.Now()
	createInvoiceApi.DueDate = "2020-09-17"
	createInvoiceApi.LineItems = lineItems

	client := &http.Client{}

	invoise, err := xero.CreateInvoice(&createInvoiceApi, client)
	if err != nil {
		log.Fatal(err)
		fmt.Fprintf(w, "Invoice not added! check the log.")
	} else {
		fmt.Fprintf(w, "Invoice Added to Xero System. Invoice Id : "+invoise.InvoiceID)
	}
}

func UploadAttachmentsXero(w http.ResponseWriter, r *http.Request) {
	attachment, err := xero.AddAttachmentToInvoice()
	if err != nil {
		log.Fatal(err)
		fmt.Fprintf(w, "Attachment not added! check the log.")
	} else {
		fmt.Fprintf(w, "Attachment added! Attachment id : %s", attachment.AttachmentID)
	}
}

func GetInvoiceXero(w http.ResponseWriter, r *http.Request) {
	invoice, err := xero.GetInvoice()
	if err != nil {
		log.Fatal(err)
		fmt.Fprintf(w, "Invoice getting error! check the log. "+err.Error())
	} else {
		fmt.Fprintf(w, "Invoice -  Invoice id : %s , Type : %s", invoice.InvoiceID, invoice.Type)
	}
}
