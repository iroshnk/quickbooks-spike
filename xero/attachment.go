package xero

type AttachmentReaponse struct {
	Id           string
	Status       string
	ProviderName string
	DateTimeUTC  string
	Attachments  []Attachment
}

type Attachment struct {
	AttachmentID  string
	FileName      string
	Url           string
	MimeType      string
	ContentLength string
}
