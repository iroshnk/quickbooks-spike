package xero

import (
	"bufio"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"quickbooks-spike/cache"
)

func CreateInvoice(inv *CreateInvoiceApi, c *http.Client) (*Invoice, error) {
	log.Println("Entering Add Invoice.")
	var u, err = url.Parse("https://api.xero.com/api.xro/2.0/Invoices")
	if err != nil {
		return nil, err
	}

	var j []byte
	j, err = json.Marshal(inv)
	if err != nil {
		return nil, err
	}

	var req *http.Request
	req, err = http.NewRequest("POST", u.String(), bytes.NewBuffer(j))
	if err != nil {
		return nil, err
	}

	//get accessToken from cache
	accessToken := cache.GetFromCache("xero_access_token")
	xeroTenantId := cache.GetFromCache("xero_tenant_id")

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", "Bearer "+accessToken)
	req.Header.Add("xero-tenant-id", xeroTenantId)

	var res *http.Response
	res, err = c.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	//print response body
	bodyBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	invo := InvoiceResponse{}
	json.Unmarshal(bodyBytes, &invo)

	//Invoice id add to cache
	cache.AddToCache("xero_added_invoice_id", invo.Invoices[0].InvoiceID)

	log.Println("Exiting Add Invoice.")
	return &invo.Invoices[0], nil
}

func AddAttachmentToInvoice() (*Attachment, error) {
	log.Println("Entering Add Xero Attachment.")
	client := &http.Client{Timeout: 180 * time.Second}
	var u, err = url.Parse("https://api.xero.com/api.xro/2.0/Invoices/" + cache.GetFromCache("xero_added_invoice_id") + "/Attachments/receipt.jpg")
	if err != nil {
		return nil, err
	}

	fileToBeUploaded := "expenses/receipt.jpg"
	// Open file on disk.
	imageFile, err := os.Open(fileToBeUploaded)
	if err != nil {
		log.Println("error: ", err)
		return nil, err
	}
	// Read entire JPEG into byte slice.
	reader := bufio.NewReader(imageFile)
	content, err := ioutil.ReadAll(reader)
	if err != nil {
		log.Println("error: ", err)
		return nil, err
	}

	var req *http.Request
	req, err = http.NewRequest("POST", u.String(), bytes.NewBuffer(content))
	if err != nil {
		return nil, err
	}

	//get accessToken from cache
	accessToken := cache.GetFromCache("xero_access_token")
	xeroTenantId := cache.GetFromCache("xero_tenant_id")

	req.Header.Add("Content-Type", "image/png")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", "Bearer "+accessToken)
	req.Header.Add("xero-tenant-id", xeroTenantId)

	var res *http.Response
	res, err = client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	//print response body
	bodyBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	attachmentRsp := AttachmentReaponse{}
	json.Unmarshal(bodyBytes, &attachmentRsp)
	log.Println(attachmentRsp)

	log.Println("Exiring Add Xero Attachment.")

	return &attachmentRsp.Attachments[0], nil
}

func GetInvoice() (*Invoice, error) {
	log.Println("Entering Get Invoice.")
	client := &http.Client{}

	req, err := http.NewRequest("GET", "https://api.xero.com/api.xro/2.0/Invoices/"+cache.GetFromCache("xero_added_invoice_id"), nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	accessToken := cache.GetFromCache("xero_access_token")
	xeroTenantId := cache.GetFromCache("xero_tenant_id")

	//set headers
	req.Header.Set("accept", "application/json")
	req.Header.Add("Authorization", "Bearer "+accessToken)
	req.Header.Add("xero-tenant-id", xeroTenantId)

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	invo := GetInvoiceResponse{}
	json.Unmarshal(bodyBytes, &invo)

	log.Println("Exiting Get Invoice.")

	return &invo.Invoices[0], nil
}
