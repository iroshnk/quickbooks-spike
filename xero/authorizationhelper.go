package xero

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"quickbooks-spike/cache"
)

func BasicAuth() string {
	//hard corded client_id and client_secret
	auth := "458DE830AA7A4E059CBE853C62BFAC23" + ":" + "gaMIKO4M_WZ39F85zcaN1WVq7ISzjrhiJgviDxCmxWlaH1TW"
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func GetAccessToken(code string, state string) (bool, error) {
	client := &http.Client{}
	data := url.Values{}

	//set parameters
	data.Set("grant_type", "authorization_code")
	data.Add("code", code)
	data.Add("redirect_uri", "http://localhost:9090/oauth2redirectXero")

	tokenEndpoint := "https://identity.xero.com/connect/token"
	request, err := http.NewRequest("POST", tokenEndpoint, bytes.NewBufferString(data.Encode()))
	if err != nil {
		log.Println(err)
		return false, err
	}

	//set headers
	request.Header.Set("accept", "application/json")
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	request.Header.Set("Authorization", "Basic "+BasicAuth())

	resp, err := client.Do(request)
	if err != nil {
		log.Println(err)
		return false, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return false, err
	}

	bearerTokenResponse, err := getBearerTokenResponse([]byte(body))

	cache.AddToCache("xero_access_token", bearerTokenResponse.AccessToken)
	cache.AddToCache("xero_refresh_token", bearerTokenResponse.RefreshToken)

	tenantRequest, err := http.NewRequest("GET", "https://api.xero.com/connections", nil)
	if err != nil {
		log.Println(err)
		return false, err
	}

	//set headers
	tenantRequest.Header.Set("accept", "application/json")
	tenantRequest.Header.Set("Authorization", "Bearer "+cache.GetFromCache("xero_access_token"))

	tenantResp, err := client.Do(tenantRequest)
	if err != nil {
		log.Println(err)
		return false, err
	}

	defer tenantResp.Body.Close()
	tenantBody, err := ioutil.ReadAll(tenantResp.Body)
	if err != nil {
		log.Println(err)
		return false, err
	}

	tenants, err := getTenantsResponse(tenantBody)
	if err != nil {
		log.Println(err)
		return false, err
	}
	cache.AddToCache("xero_tenant_id", tenants[0].TenantId)

	return true, nil
}

func RefreshToken() (bool, error) {
	log.Println("Entering RefreshToken - Xero")
	client := &http.Client{}
	data := url.Values{}

	//add parameters
	data.Set("grant_type", "refresh_token")
	refreshToken := cache.GetFromCache("xero_refresh_token")
	data.Add("refresh_token", refreshToken)

	request, err := http.NewRequest("POST", "https://identity.xero.com/connect/token", bytes.NewBufferString(data.Encode()))
	if err != nil {
		log.Fatalln(err)
		return false, err
	}
	//set the headers
	request.Header.Set("accept", "application/json")
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	request.Header.Set("Authorization", "Basic "+BasicAuth())

	resp, err := client.Do(request)
	if err != nil {
		log.Fatalln(err)
		return false, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
		return false, err
	}
	bearerTokenResponse, err := getBearerTokenResponse([]byte(body))

	cache.AddToCache("xero_access_token", bearerTokenResponse.AccessToken)
	cache.AddToCache("xero_refresh_token", bearerTokenResponse.RefreshToken)

	log.Println("Exiting RefreshToken - Xero")

	return true, nil
}

type Tenant struct {
	Id             string `json:"id"`
	AuthEventId    string `json:"authEventId"`
	TenantId       string `json:"tenantId"`
	TenantType     string `json:"tenantType"`
	TenantName     string `json:"tenantName"`
	CreatedDateUtc string `json:"createdDateUtc"`
	UpdatedDateUtc string `json:"updatedDateUtc"`
}

func getTenantsResponse(body []byte) ([]Tenant, error) {
	var arr []Tenant
	err := json.Unmarshal(body, &arr)
	if err != nil {
		log.Fatalln("error getting TenantsResponse:", err)
	}
	return arr, err
}

type BearerTokenResponse struct {
	RefreshToken           string `json:"refresh_token"`
	AccessToken            string `json:"access_token"`
	TokenType              string `json:"token_type"`
	IdToken                string `json:"id_token"`
	ExpiresIn              int64  `json:"expires_in"`
	XRefreshTokenExpiresIn int64  `json:"x_refresh_token_expires_in"`
}

func getBearerTokenResponse(body []byte) (*BearerTokenResponse, error) {
	var s = new(BearerTokenResponse)
	err := json.Unmarshal(body, &s)
	if err != nil {
		log.Fatalln("error getting BearerTokenResponse:", err)
	}
	return s, err
}
