package xero

import "encoding/json"

// InvoiceResponse for Invoice creation response
type InvoiceResponse struct {
	Id           string
	Status       string
	ProviderName string
	DateTimeUTC  string
	Invoices     []Invoice
}

type Invoice struct {
	Type            string
	Contact         Contact
	Date            string
	DueData         string
	DateString      string
	DueDateString   string
	Status          string
	LineAmountTypes string
	SubTotal        json.Number
	TotalTax        json.Number
	Total           json.Number
	UpdatedDateUTC  string
	CurrencyCode    string
	InvoiceID       string
	InvoiceNumber   string
	AmountDue       json.Number
	AmountPaid      json.Number
	AmountCredited  json.Number
}

// GetInvoiceResponse for Invoice get response
type GetInvoiceResponse struct {
	Invoices []Invoice
}

// CreateInvoiceApi for Invoice creation request
type CreateInvoiceApi struct {
	LineAmountTypes string
	Type            string

	Contact Contact

	//Date     Date
	DueDate string

	LineItems [1]LineItem
}

type Contact struct {
	Name string
}

// LineItem for Invoice creation request line item
type LineItem struct {
	Description string
	AccountCode string

	Quantity   json.Number
	UnitAmount json.Number
}
