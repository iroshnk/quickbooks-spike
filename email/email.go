package email

import (
	"crypto/tls"
	"os"

	"gopkg.in/gomail.v2"
)

func (config *EmailConfig) SendMail(message *EmailMessage) error {
	m := gomail.NewMessage()
	m.SetHeader("From", config.From)
	m.SetHeader("To", message.To)
	//m.SetAddressHeader("Cc", "cctest@gmail.com", "Irosh")
	m.SetHeader("Subject", message.Subject)
	m.SetBody("text/html", message.Body)
	//m.Attach("/path/to/attachment")

	d := gomail.NewDialer(config.SMTPHost, config.SMTPPort, config.From, config.Password)
	if os.Getenv("ENVIRONMENT") == "test" {
		d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}

	if err := d.DialAndSend(m); err != nil {
		return err
	}

	return nil
}
