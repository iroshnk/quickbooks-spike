package email

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSendEmail(t *testing.T) {
	var message = &EmailMessage{
		Subject: "Send Email Test",
		Body: `<!DOCTYPE html>
					<html lang="en">
					<head>
						<title>IDP Execution Summary Email</title>
					</head>
					<body>test email</body>
					</html>`,
		To: "totests@gmail.com",
	}

	var config = &EmailConfig{
		//SMTPHost: "smtp.gmail.com",
		//SMTPPort: "587",
		SMTPHost: "fakesmtpserver",
		SMTPPort: 1025,
		Username: "test@gmail.com",
		Password: "password",
		From:     "test@gmail.com",
	}

	err := config.SendMail(message)
	assert.NoError(t, err)

	var client = &http.Client{}

	tenantRequest, err := http.NewRequest("GET", "http://fakesmtpserver:1080/api/emails", nil)
	if err != nil {
		assert.NoError(t, err)
	}

	resp, err := client.Do(tenantRequest)
	if err != nil {
		assert.NoError(t, err)
	}

	if resp.StatusCode != 200 {

	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		assert.NoError(t, err)
	}

	var emails []email
	err2 := json.Unmarshal(body, &emails)
	if err != nil {
		assert.NoError(t, err2)
	}

	email := emails[0]

	if string(email.From.Text) != config.From {
		t.Errorf("unexpected sender: got %v want %v",
			email.From.Text, config.From)
	}

	if string(email.To.Text) != message.To {
		t.Errorf("unexpected receiver: got %v want %v",
			email.To.Text, message.To)
	}
}

type email struct {
	HTML       string         `json:"html"`
	Text       string         `json:"text"`
	TextAsHTML string         `json:"textAsHtml"`
	Subject    string         `json:"subject"`
	Date       string         `json:"date"`
	To         emailReference `json:"to"`
	From       emailReference `json:"from"`
}

type emailReference struct {
	HTML string `json:"html"`
	Text string `json:"text"`
}
