package email

type EmailConfig struct {
	SMTPHost string
	SMTPPort int
	Username string
	Password string
	From     string
}

type EmailMessage struct {
	Subject string
	Body    string
	To      string
	Cc      string
}
