module quickbooks-spike

go 1.14

require (
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/stretchr/testify v1.6.1
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
