package expenses

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"net/url"
	"os"
	"quickbooks-spike/cache"
	"time"
)

type EntityRef struct {
	Value string `json:"value"`
	Type  string `json:"type"`
}

type AttachableRef struct {
	EntityRef     EntityRef
	IncludeOnSend bool
}

type Attachable struct {
	Id              string
	FileName        string
	FileAccessUri   string
	TempDownloadUri string
	SyncToken       string
	AttachableRef   AttachableRef
}

type AttachableResponse struct {
	Attachable Attachable
}

type AttachedResponse struct {
	AttachableResponse []AttachableResponse
	time               string
}

func UploadAttachments() (*AttachableResponse, error) {

	// Metadata content.
	metadata := `{
		"AttachableRef": [
		{
		   "EntityRef": {
			 "type": "Purchase",
			 "value": "` + cache.GetFromCache("intuit_purchase_id") + `"
		   }
		}
		],
		"FileName": "receipt.jpg",
		"ContentType": "image/jpg"
	 }`

	// New multipart writer.
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// Metadata part.
	metadataHeader := textproto.MIMEHeader{}
	metadataHeader.Set("Content-Type", "application/json")
	metadataHeader.Set("Content-Disposition", "form-data; name=\"file_metadata_01\"; filename=\"attachment.json\"")
	part, err := writer.CreatePart(metadataHeader)
	if err != nil {
		log.Fatalf("Error writing metadata headers: %v", err)
		return nil, err
	}
	part.Write([]byte(metadata))

	// Media Files.
	mediaHeader := textproto.MIMEHeader{}
	mediaHeader.Set("Content-Disposition", "form-data; name=\"file_content_01\"; filename=\"receipt.jpg\"")
	mediaHeader.Set("Content-Type", "image/jpeg")

	mediaPart, err := writer.CreatePart(mediaHeader)
	if err != nil {
		log.Fatalf("Error writing media headers: %v", err)
		return nil, err
	}

	fileToBeUploaded := "expenses/receipt.jpg"
	// Open file on disk.
	imageFile, err := os.Open(fileToBeUploaded)
	if err != nil {
		log.Fatalf("error: %v", err)
		return nil, err
	}
	// Read entire JPEG into byte slice.
	reader := bufio.NewReader(imageFile)
	content, err := ioutil.ReadAll(reader)
	if err != nil {
		log.Fatalf("error: %v", err)
		return nil, err
	}

	mediaPart.Write([]byte(content))

	// Close multipart writer.
	if err := writer.Close(); err != nil {
		log.Fatalf("Error closing multipart writer: %v", err)
		return nil, err
	}

	// Request Content-Type with boundary parameter.
	contentType := fmt.Sprintf("multipart/form-data; boundary=%s", writer.Boundary())

	// Initialize HTTP Request and headers.
	var u, errUrl = url.Parse("https://sandbox-quickbooks.api.intuit.com")
	if err != nil {
		//return nil, errUrl
		log.Fatalf("Error making a url: %v", errUrl)
		return nil, err
	}
	RealmID := cache.GetFromCache("realmID")

	u.Path = "/v3/company/" + RealmID + "/upload"
	var v = url.Values{}
	v.Add("minorversion", minorVersion)
	u.RawQuery = v.Encode()
	uploadURL := u.String()
	r, err := http.NewRequest(http.MethodPost, uploadURL, bytes.NewReader(body.Bytes()))
	if err != nil {
		log.Fatalf("Error initializing a request: %v", err)
		return nil, err
	}
	//get accessToken from cache
	accessToken := cache.GetFromCache("access_token")

	r.Header.Set("Content-Type", contentType)
	r.Header.Add("Accept", "application/json")
	r.Header.Add("Authorization", "Bearer "+accessToken)

	// HTTP Client.
	client := &http.Client{Timeout: 180 * time.Second}
	rsp, err := client.Do(r)
	if err != nil {
		log.Fatalf("Error making a request: %v", err)
		return nil, err
	}

	// Check response status code.
	if rsp.StatusCode != http.StatusOK {
		log.Println("Request failed with response code: " + rsp.Status)

	} else {
		log.Println("Request was a success")
	}

	//print response body
	bodyBytes, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	attachment := AttachedResponse{}
	json.Unmarshal(bodyBytes, &attachment)

	log.Println("Exiting Get Purchase(Expenses).")

	return &attachment.AttachableResponse[0], nil
}
