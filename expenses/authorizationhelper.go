package expenses

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"quickbooks-spike/cache"
)

func basicAuth() string {
	//hard corded client_id and client_secret
	auth := "ABDsNeoQ3A076liDCJpPbda9GqYzQxRIanOPpGXsjunWOksEXr" + ":" + "hkGXjDI27v3TREkCx8cSU8YPFfHgMdSf2Okf6923"
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func GetAccessToken(code string, state string) (bool, error) {
	client := &http.Client{}
	data := url.Values{}

	//set parameters
	data.Set("grant_type", "authorization_code")
	data.Add("code", code)
	data.Add("redirect_uri", "http://localhost:9090/oauth2redirect")

	tokenEndpoint := "https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer"
	request, err := http.NewRequest("POST", tokenEndpoint, bytes.NewBufferString(data.Encode()))
	if err != nil {
		log.Println(err)
		return false, err
	}

	//set headers
	request.Header.Set("accept", "application/json")
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	request.Header.Set("Authorization", "Basic "+basicAuth())

	resp, err := client.Do(request)
	if err != nil {
		log.Println(err)
		return false, err
	}
	fmt.Println(resp.Status)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return false, err
	}

	bearerTokenResponse, err := getBearerTokenResponse([]byte(body))

	cache.AddToCache("access_token", bearerTokenResponse.AccessToken)
	cache.AddToCache("refresh_token", bearerTokenResponse.RefreshToken)

	return true, nil
}

func RefreshToken() (bool, error) {
	log.Println("Entering RefreshToken ")
	client := &http.Client{}
	data := url.Values{}

	//add parameters
	data.Set("grant_type", "refresh_token")
	refreshToken := cache.GetFromCache("refresh_token")
	data.Add("refresh_token", refreshToken)

	request, err := http.NewRequest("POST", "https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer", bytes.NewBufferString(data.Encode()))
	if err != nil {
		log.Fatalln(err)
		return false, err
	}
	//set the headers
	request.Header.Set("accept", "application/json")
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	request.Header.Set("Authorization", "Basic "+basicAuth())

	resp, err := client.Do(request)
	if err != nil {
		log.Fatalln(err)
		return false, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
		return false, err
	}
	bearerTokenResponse, err := getBearerTokenResponse([]byte(body))

	cache.AddToCache("access_token", bearerTokenResponse.AccessToken)
	cache.AddToCache("refresh_token", bearerTokenResponse.RefreshToken)

	return true, nil
}

type BearerTokenResponse struct {
	RefreshToken           string `json:"refresh_token"`
	AccessToken            string `json:"access_token"`
	TokenType              string `json:"token_type"`
	IdToken                string `json:"id_token"`
	ExpiresIn              int64  `json:"expires_in"`
	XRefreshTokenExpiresIn int64  `json:"x_refresh_token_expires_in"`
}

func getBearerTokenResponse(body []byte) (*BearerTokenResponse, error) {
	var s = new(BearerTokenResponse)
	err := json.Unmarshal(body, &s)
	if err != nil {
		log.Fatalln("error getting BearerTokenResponse:", err)
	}
	return s, err
}
