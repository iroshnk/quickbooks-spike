package expenses

// EndpointURL specifies the endpoint to connect to.
type EndpointURL string

const (
	// ProductionEndpoint is for live apps.
	ProductionEndpoint EndpointURL = "https://quickbooks.api.intuit.com"
	// SandboxEndpoint is for testing.
	SandboxEndpoint EndpointURL = "https://sandbox-quickbooks.api.intuit.com"
)

const minorVersion = "53"
