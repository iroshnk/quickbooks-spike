package expenses

import (
	"io/ioutil"
	"log"
	"quickbooks-spike/cache"

	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
)

// Purchase represents a QuickBooks Purchase object.
type Purchase struct {
	ID        string `json:"Id"`
	SyncToken string
	domain    string
	MetaData  MetaData

	PurchaseEx PurchaseEx

	TxnDate     Date
	TotalAmt    json.Number
	PaymentType string
	sparse      bool
	AccountRef  ReferenceType

	//LinkedTxn
	Line []Line

	CustomField []string
}

// MetaData is a timestamp of genesis and last change of a Quickbooks object
type MetaData struct {
	CreateTime      Date
	LastUpdatedTime Date
}

// ReferenceType represents a QuickBooks reference to another object.
type ReferenceType struct {
	Value string `json:"value"`
	Name  string `json:"name"`
}

// ValueReferenceType represents a QuickBooks reference to another object.
type ValueReferenceType struct {
	Value string `json:"value"`
}

// PurchaseEx ...
type PurchaseEx struct {
	any []PurchaseExAny
}

// PurchaseExAny ...
type PurchaseExAny struct {
	name string
	//nil bool
	value           ReferenceType
	declaredType    string
	scope           string
	globalScope     bool
	typeSubstituted bool
}

// Line ...
type Line struct {
	ID                            string
	Amount                        json.Number
	PaymentType                   string
	AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail
}

// AccountBasedExpenseLineDetail ...
type AccountBasedExpenseLineDetail struct {
	//TaxCodeRef     ValueReferenceType
	//BillableStatus string
	AccountRef ReferenceType
}

// PurchaseCreateApi ...
type PurchaseCreateApi struct {
	PaymentType string
	AccountRef  ReferenceType

	Line [1]PurchaseCreateLineApi
}

//PurchaseCreateLineApi ...
type PurchaseCreateLineApi struct {
	Amount                        json.Number
	DetailType                    string
	AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail
}

type GetPurchaseResponse struct {
	Purchase Purchase
}

// CreatePurchase creates the given Purchase on the QuickBooks server, returning
// the resulting Purchase object.
func CreatePurchase(inv *PurchaseCreateApi, c *http.Client) (*Purchase, error) {
	log.Println("Entering Add Purchase(Expenses).")
	var u, err = url.Parse("https://sandbox-quickbooks.api.intuit.com")
	if err != nil {
		return nil, err
	}
	RealmID := cache.GetFromCache("realmID")

	u.Path = "/v3/company/" + RealmID + "/purchase"
	var v = url.Values{}
	v.Add("minorversion", minorVersion)
	u.RawQuery = v.Encode()
	var j []byte
	j, err = json.Marshal(inv)
	if err != nil {
		return nil, err
	}

	var req *http.Request
	req, err = http.NewRequest("POST", u.String(), bytes.NewBuffer(j))
	if err != nil {
		return nil, err
	}

	//get accessToken from cache
	accessToken := cache.GetFromCache("access_token")

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", "Bearer "+accessToken)

	var res *http.Response
	res, err = c.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	//print response body
	//bodyBytes, err := ioutil.ReadAll(res.Body)
	//if err != nil {
	//	log.Fatal(err)
	//}
	//bodyString := string(bodyBytes)
	//log.Println(bodyString)

	if res.StatusCode != http.StatusOK {
		return nil, errors.New("Error")
	}

	var r struct {
		Purchase Purchase
		time     Date
	}
	err = json.NewDecoder(res.Body).Decode(&r)
	if err != nil {
		log.Fatal(err)
	}

	cache.AddToCache("intuit_purchase_id", r.Purchase.ID)

	log.Println("Exiting Add Purchase(Expenses).")
	return &r.Purchase, err
}

func GetPurchase() (*Purchase, error) {
	log.Println("Entering Get Purchase(Expenses).")
	client := &http.Client{}

	var u, err = url.Parse("https://sandbox-quickbooks.api.intuit.com")
	if err != nil {
		return nil, err
	}
	RealmID := cache.GetFromCache("realmID")

	u.Path = "/v3/company/" + RealmID + "/purchase/" + cache.GetFromCache("intuit_purchase_id")

	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	accessToken := cache.GetFromCache("access_token")

	//set headers
	req.Header.Set("accept", "application/json")
	req.Header.Add("Authorization", "Bearer "+accessToken)

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	purchase := GetPurchaseResponse{}
	json.Unmarshal(bodyBytes, &purchase)

	log.Println("Exiting Get Purchase(Expenses).")

	return &purchase.Purchase, nil
}
